package com.example.hrproject.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class DepartmentDto {

    private Long id;
    private String departmentName;
}
