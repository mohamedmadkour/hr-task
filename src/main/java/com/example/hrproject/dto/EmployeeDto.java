package com.example.hrproject.dto;

import com.example.hrproject.model.Department;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;


@Builder
@Setter
@Getter
public class EmployeeDto {

    private long id;
    private String employeeName;
    private String phoneNumber;
    private Double salary;
    private DepartmentDto department = new DepartmentDto();

}
