package com.example.hrproject.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;


@Builder
@Setter
@Getter
public class EmployeeSimpleDto {

    private long id;
    private String employeeName;


}
