package com.example.hrproject.controller;

import com.example.hrproject.dto.DepartmentDto;
import com.example.hrproject.mapper.DepartmentMapper;
import com.example.hrproject.service.DepartmentService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("api/hr/department")
public class DepartmentController {

    private final DepartmentService departmentService;

    private final DepartmentMapper departmentMapper;

    @PostMapping("")
    public DepartmentDto insert(@RequestBody DepartmentDto dto) {
        return departmentMapper.map(departmentService.insert(departmentMapper.unMap(dto)));
    }

    @PutMapping("")
    public DepartmentDto update(@RequestBody DepartmentDto dto) {
        return departmentMapper.map(departmentService.update(departmentMapper.unMap(dto)));
    }

    @GetMapping ("/id/{deptId}")
    public DepartmentDto findById(@PathVariable Long deptId) {
        return departmentMapper.map(departmentService.findById(deptId));
    }

    @GetMapping ("/findAll")
    public List<DepartmentDto> findAll() {
        return departmentMapper.map(departmentService.findAll());
    }

    @GetMapping ("/name")
    public List<DepartmentDto> findByName(@RequestParam String name) {
        return departmentMapper.map(departmentService.findByDepartmentName(name));
    }
    @DeleteMapping ("/delete/{deptId}")
    public String deleteById(@PathVariable Long deptId) {
        departmentService.deleteById(deptId);
        return "Delete Department With ID : ( " + deptId + " )Done";
    }
}
