package com.example.hrproject.controller;

import com.example.hrproject.dto.EmployeeDto;
import com.example.hrproject.dto.EmployeeSimpleDto;
import com.example.hrproject.mapper.EmployeeMapper;
import com.example.hrproject.model.Employee;
import com.example.hrproject.model.SearchRequest;
import com.example.hrproject.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@RestController
@RequestMapping("api/hr/employee")
public class EmployeeController {

    private final EmployeeService employeeService;
    private final EmployeeMapper employeeMapper;


    @PostMapping("")
    public EmployeeDto insert(@RequestBody EmployeeDto dto) {
        Employee entity = employeeService.update(employeeMapper.unMap(dto));
        return employeeMapper.map(entity);
    }

    @PutMapping("")
    public EmployeeDto update(@RequestBody EmployeeDto dto) {
        Employee entity = employeeService.update(employeeMapper.unMap(dto));
        return employeeMapper.map(entity);
    }

    @GetMapping ("/id/{deptId}")
    public EmployeeSimpleDto findById(@PathVariable Long deptId) {
        return employeeMapper.mapToSimple(employeeService.findById(deptId));
    }

    @GetMapping ("/findAll")
    public List<EmployeeDto> findAll() {
        return employeeMapper.map(employeeService.findAll());
    }

    @DeleteMapping ("/delete/{employeeId}")
    public String deleteById(@PathVariable Long employeeId) {
         employeeService.deleteById(employeeId);
        return "Delete Employee With ID : ( " + employeeId + " )Done";
    }

    @GetMapping  ("/name")
    public List<EmployeeDto> findByName(@RequestParam String name) {
        return employeeMapper.map(employeeService.findByEmployeeName(name));
    }

    @PostMapping("/filter")
    public Page<EmployeeDto> filter(@RequestBody  Optional<SearchRequest> req) {
        return employeeService.filter(req).map(t -> employeeMapper.map(t));
    }
}
