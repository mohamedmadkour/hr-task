package com.example.hrproject.mapper;

import com.example.hrproject.dto.EmployeeDto;
import com.example.hrproject.dto.EmployeeSimpleDto;
import com.example.hrproject.model.Employee;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(uses = {DepartmentMapper.class})
public interface EmployeeMapper extends BaseMapper<Employee, EmployeeDto>{

    EmployeeSimpleDto mapToSimple(Employee t);

}
