package com.example.hrproject.mapper;

import com.example.hrproject.dto.DepartmentDto;
import com.example.hrproject.dto.EmployeeSimpleDto;
import com.example.hrproject.model.Department;
import com.example.hrproject.model.Employee;
import org.mapstruct.Mapper;

@Mapper
public interface DepartmentMapper extends BaseMapper<Department, DepartmentDto>{

}
