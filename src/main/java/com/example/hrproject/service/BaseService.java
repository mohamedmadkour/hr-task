package com.example.hrproject.service;

import jakarta.persistence.MappedSuperclass;

import java.util.List;
import java.util.Optional;

@MappedSuperclass
public  interface BaseService<T, ID> {

    default List<T> findAll() {
        return null;
    }

    T findById(ID id);


    Optional<T> getById(ID id);

    T insert(T entity);


    T persist(T entity);

    T update(T entity);

    List<T> saveAll(List<T> entities);


    void deleteById(ID id);


}
