package com.example.hrproject.service;


import com.example.hrproject.model.Department;
import java.util.List;

public interface DepartmentService extends BaseService<Department, Long>{

    List<Department> findByDepartmentName(String name);
}
