package com.example.hrproject.service.Impl;

import com.example.hrproject.model.Department;
import com.example.hrproject.model.Employee;
import com.example.hrproject.model.SearchRequest;
import com.example.hrproject.repository.EmployeeRepository;
import com.example.hrproject.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public  class EmployeeServiceImpl extends BaseServiceImpl<Employee, Long> implements EmployeeService {


    private final EmployeeRepository employeeRepository;
    private final DepartmentServiceImpl departmentService;


    public List<Employee> findAll() {
        return super.findAll();
    }

    public Employee findById(Long employeeId) {
        return super.findById(employeeId);
    }

    @Override
    public Employee insert(Employee entity) {
        Department department = departmentService.findById(entity.getDepartment().getId());
        entity.setDepartment(department);
        entity = super.insert(entity);
        return entity;
    }

    @Override
    public Employee persist(Employee entity) {
        return null;
    }

    @Override
    public Employee update(Employee entity) {
        Department department = departmentService.findById(entity.getDepartment().getId());
        entity.setDepartment(department);
        entity = super.update(entity);
        return entity;
    }

    @Override
    public List<Employee> saveAll(List entities) {
        return null;
    }

//    @Override
    public void deleteById(Long employeeId) {
        super.deleteById(employeeId);
    }

    public List<Employee> findByEmployeeName(String name) {
        return employeeRepository.findByEmployeeName(name);
    }

    public Page<Employee> filter(Optional<SearchRequest> request) {

        SearchRequest req = request.orElse(new SearchRequest());
        Sort sort = req.getSortableType() == 1 ? Sort.by(req.getSortableColumn()).ascending() : Sort.by(req.getSortableColumn()).descending();
        Pageable pageable = PageRequest.of(req.getPageNumber(), req.getPageSize(), sort);
        String name = null;
        Number salaryFrom = null;
        Number salaryTo = null;
        Number employeeId = null;
        Number departmentId = null;

        if (!req.getSearchCriteria().isEmpty()) {
            name = (String) req.getSearchCriteria().getOrDefault("name", null);
            salaryFrom = (Number) req.getSearchCriteria().getOrDefault("salaryFrom", null);
            salaryTo = (Number) req.getSearchCriteria().getOrDefault("salaryTo", null);
            employeeId = (Number) req.getSearchCriteria().getOrDefault("employeeId", null);
            departmentId = (Number) req.getSearchCriteria().getOrDefault("departmentId", null);

        }
        Page<Employee> res =
          this.employeeRepository.filter(name, salaryFrom != null? salaryFrom.doubleValue(): null
                 ,salaryTo!=null? salaryTo.doubleValue() : null
                 ,employeeId !=null? employeeId.longValue():null
                 ,departmentId != null ? departmentId.longValue() :null , pageable);
        res.getTotalElements();

        return res;
    }

}
