package com.example.hrproject.service.Impl;

import com.example.hrproject.dto.EmployeeDto;
import com.example.hrproject.dto.EmployeeSimpleDto;
import com.example.hrproject.model.Department;
import com.example.hrproject.model.Employee;
import com.example.hrproject.model.SearchRequest;
import com.example.hrproject.repository.DepartmentRepository;
import com.example.hrproject.service.DepartmentService;
import com.example.hrproject.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
@RequiredArgsConstructor
public  class DepartmentServiceImpl extends BaseServiceImpl<Department, Long> implements DepartmentService {

    @Autowired
    private DepartmentRepository departmentRepository;


    public Department insert(Department entity){
        return departmentRepository.save(entity);
    }

    @Override
    public Department persist(Department entity) {
        return null;
    }

    public Department update(Department entity){
        return departmentRepository.save(entity);
    }
    public Department findById(Long deptId){
        return departmentRepository.findById(deptId).orElse(null);
    }

    public List<Department> findAll(){
        return departmentRepository.findAll();
    }

    @Override
    public List<Department> findByDepartmentName(String name) {
        return departmentRepository.findByDepartmentName(name);
    }
}
