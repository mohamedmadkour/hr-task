package com.example.hrproject.service.Impl;


import com.example.hrproject.repository.base.BaseRepository;
import com.example.hrproject.service.BaseService;
import jakarta.persistence.MappedSuperclass;
import org.springframework.beans.factory.annotation.Autowired;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;


/**
 * @param <T>
 * @param <ID>
 */
@MappedSuperclass
public abstract class BaseServiceImpl<T, ID extends Serializable> implements BaseService<T, ID> {


	@Autowired
	private BaseRepository<T, ID> baseRepository;



	/**
	 * @param id
	 * @return
	 */
	@Override
	public T findById(ID id) {
		String params [] = {id.toString()};
		return baseRepository.findById(id)
				.orElseThrow(() -> new RuntimeException(" Employee With This " + id + " Id not Found"));
	}

	@Override
	public Optional<T> getById(ID id) {
		// TODO Auto-generated method stub
		return baseRepository.findById(id);
	}

	/**
	 * @param entity
	 * @return
	 */
	@Override
	public T insert(T entity) {
		return baseRepository.save(entity);

	}

	@Override
	public T update(T entity) {
		return baseRepository.save(entity);
	}

	/**
	 * @param entities
	 * @return
	 */
	@Override
	public List<T> saveAll(List<T> entities) {
		return baseRepository.saveAll(entities);
	}

	/**
	 * @param id
	 * @return
	 */
	@Override
	public void deleteById(ID id) {
		baseRepository.deleteById(id);
	}

}
