package com.example.hrproject.service;

import com.example.hrproject.model.Employee;
import com.example.hrproject.model.SearchRequest;
import org.springframework.data.domain.Page;
import java.util.List;
import java.util.Optional;


public interface EmployeeService extends BaseService<Employee, Long>{

    Employee insert(Employee entity);

    Employee update(Employee entity);
    Page<Employee> filter(Optional<SearchRequest> request);
    List<Employee> findByEmployeeName(String name);
}
