package com.example.hrproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({ "com.example.hrproject.*" })
public class HrProjectApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {

        SpringApplication.run(HrProjectApplication.class, args);
    }

}
