package com.example.hrproject.model;

import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;
import lombok.Getter;
import lombok.Setter;


@Setter
@Getter
@MappedSuperclass
public abstract class BaseEntity<ID> {



	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	public BaseEntity() {
		super();
		// TODO Auto-generated constructor stub
	}
}
