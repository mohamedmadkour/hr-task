package com.example.hrproject.model;

import jakarta.persistence.*;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;


@Setter
@Getter
@Entity
@Table(name = "hr_employees")
public class Employee extends BaseEntity<Long>{

    private String employeeName;
    private String phoneNumber;
    private Double salary;

    @ManyToOne()
    @JoinColumn(name = "department_id")
    private Department department;

    public Employee(String employeeName, Double salary, String phoneNumber) {
        this.employeeName = employeeName;
        this.phoneNumber = phoneNumber;
        this.salary = salary;
    }
    public Employee(String employeeName, String phoneNumber, Double salary, Department department) {
        this.employeeName = employeeName;
        this.phoneNumber = phoneNumber;
        this.salary = salary;
        this.department = department;
    }

    public Employee(long id, String employeeName, String phoneNumber, Double salary, Department department) {
        id = id;
        this.employeeName = employeeName;
        this.phoneNumber = phoneNumber;
        this.salary = salary;
        this.department = department;
    }

    public Employee() {

    }
}
