package com.example.hrproject.model;


import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;

@Setter
@Getter
@Entity
@NoArgsConstructor
@Table(name = "hr_departments")
public class Department extends BaseEntity<Long> {

    private String departmentName;

    public Department(Long id , String departmentName) {

        super.setId(id);
        this.departmentName = departmentName;
    }

    public Department(String departmentName) {
        this.departmentName = departmentName;
    }

    public Department(Long id) {
    }
}
