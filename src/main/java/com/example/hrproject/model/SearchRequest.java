package com.example.hrproject.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.HashMap;
import java.util.Map;


//@Builder
@ToString
@Setter
@Getter
public class SearchRequest {

    private Integer pageNumber = 0;
    private Integer pageSize = 10;
    private String sortableColumn = "id" ;
    private Integer sortableType = 2 ;    //  1 for ascending , 2 for descending
    private Map<String, Object> searchCriteria = new HashMap<String, Object>();

}
