package com.example.hrproject.repository;

import com.example.hrproject.model.Employee;
import com.example.hrproject.repository.base.BaseRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EmployeeRepository extends BaseRepository<Employee, Long> {


    @Query("select emp from Employee  emp where upper(emp.employeeName) like UPPER('%' || :name || '%' ) ")
    List<Employee> findByEmployeeName(String name);

    Employee findEmployeeById(Long employeeId);

    @Query(" select  employee from Employee  employee " +
            " join employee.department dept" +
            " where (:name is null or ( (upper(employee.employeeName) like UPPER('%' || :name || '%' ) )" +
            " or (upper(employee.phoneNumber) like UPPER('%' || :name || '%' ))" +
            " or (upper(dept.departmentName) like UPPER('%' || :name || '%' )  )) )" +
            " and (:salaryFrom is null or employee.salary >= :salaryFrom) " +
            " and (:salaryTo is null or employee.salary <= :salaryTo) " +
            " and (:departmentId is null or dept.id = :departmentId)" +
            " and (:employeeId is null or employee.id = :employeeId)" )
    Page<Employee> filter(String name , Double salaryFrom , Double salaryTo , Long employeeId , Long departmentId, Pageable pageable);
}
