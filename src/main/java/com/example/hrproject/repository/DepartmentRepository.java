package com.example.hrproject.repository;

import com.example.hrproject.model.Department;
import com.example.hrproject.model.Employee;
import com.example.hrproject.repository.base.BaseRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DepartmentRepository extends BaseRepository<Department, Long> {

    @Query("select dept from Department  dept where upper(dept.departmentName) like UPPER('%' || :name || '%' ) ")
    List<Department> findByDepartmentName(String name);
}
